import React from "react";
import { shallow, mount, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import "@testing-library/jest-dom";
import { createSerializer } from "enzyme-to-json";

// Component
import App, { useTodos } from "./App";

expect.addSnapshotSerializer(createSerializer({ mode: "deep" }));

configure({ adapter: new Adapter() });

describe("useTodos", () => {
  test("comprueba que addTodo agrega un nuevo todo en el state", () => {
    function TestComponent(props) {
      const hook = props.useTodos();
      return <div {...hook}></div>;
    }
    const newTodo = "nuevo todo";
    const wrapper = shallow(<TestComponent useTodos={useTodos} />);
    let props = wrapper.find("div").props();
    props.addTodo(newTodo);
    props = wrapper.find("div").props();
    expect(props.todos[0]).toEqual({ text: newTodo });
  });
  test("comprueba que completeTodo cambia a true un todo existente", () => {
    function TestComponent(props) {
      const hook = props.useTodos();
      return <div {...hook}></div>;
    }
    const wrapper = shallow(<TestComponent useTodos={useTodos} />);
    let props = wrapper.find("div").props();
    props.completeTodo(0);
    props = wrapper.find("div").props();
    expect(props.todos[0]).toEqual({
      text: "Todo 1",
      isCompleted: true,
    });
  });
  test("comprueba que removeTodo, elimina un todo del state", () => {
    function TestComponent(props) {
      const hook = props.useTodos();
      return <div {...hook}></div>;
    }
    const wrapper = shallow(<TestComponent useTodos={useTodos} />);
    let props = wrapper.find("div").props();
    props.removeTodo(0);
    props = wrapper.find("div").props();
    expect(props.todos).toEqual([
      {
        text: "Todo 2",
        isCompleted: false,
      },
      {
        text: "Todo 3",
        isCompleted: false,
      },
    ]);
  });
});

describe('Todo', () => {
  test('Renderiza el componente completo, y agrega un nuevo todo', () => {

    const prevent = jest.fn();

    const wrapper = mount(<App />);

    wrapper.find('input').simulate('change', { target: { value: "nuevo todo!"}});
    wrapper.find('form').simulate('submit', { preventDefault: prevent});
    const respuesta = wrapper.find('#todos').at(0).text().includes("nuevo todo!");
    expect(respuesta).toEqual(true);
    expect(prevent.mock.calls).toEqual([[]]);
  });
});
