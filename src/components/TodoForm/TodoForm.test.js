import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import '@testing-library/jest-dom';
import {createSerializer} from 'enzyme-to-json';
 
// Component
import TodoForm, { useTodoForm } from "./TodoForm";

expect.addSnapshotSerializer(createSerializer({mode: 'deep'}));



configure({ adapter: new Adapter() });

describe("TodoForm", () => {
  test("comprobar que el componente TodoForm no ha cambiado", () => {
    const wrapper = shallow(<TodoForm />);
    expect(wrapper).toMatchSnapshot();
  })

  test("ejecuta addTodo cuando el formulario es enviado", () => {
    const addTodo = jest.fn();
    const e = {
      target: {
        value: "nuevo todo",
      },
      preventDefault: jest.fn(),
    };

    const wrapper = shallow(<TodoForm addTodo={addTodo} />);
    wrapper.find("input").simulate("change", e);
    wrapper.find("form").simulate("submit", e);

    expect(addTodo.mock.calls).toEqual([[e.target.value]]);
    expect(e.preventDefault.mock.calls).toEqual([[]]);
  });
});

describe("useTodoForm", () => {
  test('cuando se llama a handleChange, se modifica el state "value"', () => {
    const addTodo = jest.fn();
    const e = {
      target: {
        value: "nuevo todo",
      },
      preventDefault: jest.fn(),
    };

    const Test = (props) => {
      const hook = props.hook(addTodo);
      return <div {...hook}></div>;
    };

    const wrapper = shallow(<Test hook={useTodoForm} />);
    let props = wrapper.find("div").props();
    props.handleChange(e);
    props = wrapper.find("div").props();
    expect(props.value).toEqual(e.target.value);
    expect(addTodo.mock.calls).toEqual([]);
    expect(e.preventDefault.mock.calls).toEqual([]);
  });

  test("cuando el state 'value' contiene datos, se ejecuta handleSubmit", () => {
    const addTodo = jest.fn();
    const e = {
      target: {
        value: "nuevo todo",
      },
      preventDefault: jest.fn(),
    };

    const Test = (props) => {
      const hook = props.hook(addTodo);
      return <div {...hook}></div>;
    };

    const wrapper = shallow(<Test hook={useTodoForm} />);
    let props = wrapper.find("div").props();
    props.handleChange(e);
    props = wrapper.find("div").props();
    props.handleSubmit(e);
    props = wrapper.find("div").props();

    expect(e.preventDefault.mock.calls).toEqual([[]]);
    expect(addTodo.mock.calls).toEqual([[ e.target.value ]]);
    expect(props.value).toEqual("");
  });

  test("cuando el state 'value' NO contiene datos, handleSubmit falla", () => {
    const addTodo = jest.fn();
    const e = {
      target: {
        value: "nuevo todo",
      },
      preventDefault: jest.fn(),
    };

    const Test = (props) => {
      const hook = props.hook(addTodo);
      return <div {...hook}></div>;
    };

    const wrapper = shallow(<Test hook={useTodoForm} />);
    let props = wrapper.find("div").props();
    props.handleSubmit(e);
    props = wrapper.find("div").props();

    expect(e.preventDefault.mock.calls).toEqual([[]]);
    expect(addTodo.mock.calls).toEqual([]);
    expect(props.value).toEqual("");
  });
});